# Crassipes
Okapilib but for [Rust...](https://gitlab.com/qvex/vex-rt-template) hopefully

## why?

I needed to write a readme to get rid of the embarassing template on this public repo

## No I mean why are you trying to rewrite okapilib in Rust?

Oh, I like Okapilib and I like Rust, and as Heinz Doofenshmirtz said "I loved these two things so much I wondered if combining them would exponentially increase my enjoyment of them." Of course, this project is no triple scoop garlic roach cone, but it is in development unless the commit history indicates otherwise.

## Whats with the name?

Okapis are striped animals, and Rust's mascot is a crab, so naturally, I decided the striped shore crab, Pachygrapsus Crassipes (thats right, I chose the short part of its name) to name this crate after

## Design philosophy

1. Follow the [Rust style guide](https://doc.rust-lang.org/1.0.0/style/README.html)

2. Follow to some degree the structure and naming of Okapilib, because you can't write your own code, Drew

3. I made a units library, so use the units library everywhere you can. This also means no encoderunits for you, you have to use QAngle.