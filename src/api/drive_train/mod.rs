use crate::prelude::QVoltage;

mod holonomic_drive_train;
mod skid_steer_drive;
mod x_drive;

pub use holonomic_drive_train::HolonomicDriveTrain;
pub use skid_steer_drive::SkidSteerDriveModel;
pub use x_drive::XDriveModel;

pub trait DriveTrain {
    /// Drive with voltage
    fn drive_voltage(&mut self, forward: QVoltage, yaw: QVoltage);
    /// Drive with the controller mapping of float values using arcade inputs
    fn drive_arcade(&mut self, forward: f64, yaw: f64);
}
