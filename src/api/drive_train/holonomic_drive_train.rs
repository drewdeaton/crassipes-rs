use crate::api::drive_train::DriveTrain;
use crate::prelude::QVoltage;

pub trait HolonomicDriveTrain: DriveTrain {
    /// Drive holonomically with voltage mapped to i8
    fn drive_voltage_x(&mut self, forward: QVoltage, yaw: QVoltage, right: QVoltage);
}
