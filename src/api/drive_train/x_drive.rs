use crate::api::drive_train::holonomic_drive_train::HolonomicDriveTrain;
use crate::api::drive_train::DriveTrain;
use crate::units::QVoltage;
use alloc::boxed::Box;
use crate::devices::Motor;

pub struct XDriveModel {
    left_front: Motor,
    right_front: Motor,
    left_rear: Motor,
    right_rear: Motor,
}

impl DriveTrain for XDriveModel {
    fn drive_voltage(&mut self, forward: QVoltage, yaw: QVoltage) {
        self.left_front.move_voltage(forward + yaw);
        self.right_front.move_voltage(-forward + yaw);
        self.left_rear.move_voltage(forward + yaw);
        self.right_rear.move_voltage(-forward + yaw);
    }

    fn drive_arcade(&mut self, forward: f64, yaw: f64) {
        self.left_front.move_cmap(forward + yaw);
        self.right_front.move_cmap(-forward + yaw);
        self.left_rear.move_cmap(forward + yaw);
        self.right_rear.move_cmap(-forward + yaw);
    }
}

impl HolonomicDriveTrain for XDriveModel {
    fn drive_voltage_x(&mut self, forward: QVoltage, yaw: QVoltage, right: QVoltage) {
        self.left_front.move_voltage(forward + yaw + right);
        self.right_front.move_voltage(-forward + yaw + right);
        self.left_rear.move_voltage(forward + yaw - right);
        self.right_rear.move_voltage(-forward + yaw - right);
    }
}
