use crate::api::drive_train::DriveTrain;
use crate::devices::Motor;
use crate::prelude::QVoltage;
use alloc::boxed::Box;

pub struct SkidSteerDriveModel {
    left: Motor,
    right: Motor,
}

impl SkidSteerDriveModel {
    pub fn new(left: Motor, right: Motor) -> Self {
        Self {
            left, right
        }
    }
}

impl DriveTrain for SkidSteerDriveModel {

    fn drive_voltage(&mut self, forward: QVoltage, yaw: QVoltage) {
        self.left.move_voltage(forward + yaw);
        self.right.move_voltage(forward - yaw);
    }

    fn drive_arcade(&mut self, forward: f64, yaw: f64) {
        self.left.move_cmap(forward + yaw);
        self.right.move_cmap(forward - yaw);
    }
}
