pub trait AbstractController<T> {
    /// Function to step the controller
    fn step(&mut self);

    /// Get the output from the controller, of type T
    fn get(&self) -> T;
}
