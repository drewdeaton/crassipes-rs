use alloc::string::ToString;
use crs_bind::bindings;
use crs_bind::error::Error;
/// An enum representing controller types
pub enum ControllerType {
    /// The main controller
    Primary,
    /// The partner controller
    Partner,
}

/// An object representing a Vex V5 controller
pub struct Controller {
    /// The controller type (Primary or Partner)
    controller_type: bindings::controller_id_e_t,
}

/// A module for containing the controller's inputs
pub mod controller_input {
    /// The controller's buttons
    pub enum Digital {
        A,
        B,
        X,
        Y,
        Up,
        Down,
        Left,
        Right,
        L1,
        R1,
        L2,
        R2,
    }

    /// Analog axes
    pub enum Analog {
        /// The left joystick's X axis
        LeftX,
        /// The left joystick's Y axis
        LeftY,
        /// The right joystick's X axis
        RightX,
        /// The right joystick's Y axis
        RightY,
    }
}

impl Controller {
    /// Constructs a new controller object
    #[allow(dead_code)]
    pub fn new(itype: ControllerType) -> Self {
        Controller {
            controller_type: match itype {
                ControllerType::Primary => bindings::controller_id_e_t_E_CONTROLLER_MASTER,
                ControllerType::Partner => bindings::controller_id_e_t_E_CONTROLLER_PARTNER,
            },
        }
    }

    /// Checks if a controller button is being pressed
    #[allow(dead_code)]
    pub fn get_digital(&self, button: controller_input::Digital) -> Result<bool, Error> {
        match button {
            controller_input::Digital::A => match unsafe {
                bindings::controller_get_digital(
                    self.controller_type,
                    bindings::controller_digital_e_t_E_CONTROLLER_DIGITAL_A,
                )
            } {
                0 => Ok(false),
                1 => Ok(true),
                _ => Err(Error::Custom("Button press error".to_string())),
            },
            controller_input::Digital::B => match unsafe {
                bindings::controller_get_digital(
                    self.controller_type,
                    bindings::controller_digital_e_t_E_CONTROLLER_DIGITAL_B,
                )
            } {
                0 => Ok(false),
                1 => Ok(true),
                _ => Err(Error::Custom("Button press error".to_string())),
            },
            controller_input::Digital::X => match unsafe {
                bindings::controller_get_digital(
                    self.controller_type,
                    bindings::controller_digital_e_t_E_CONTROLLER_DIGITAL_X,
                )
            } {
                0 => Ok(false),
                1 => Ok(true),
                _ => Err(Error::Custom("Button press error".to_string())),
            },
            controller_input::Digital::Y => match unsafe {
                bindings::controller_get_digital(
                    self.controller_type,
                    bindings::controller_digital_e_t_E_CONTROLLER_DIGITAL_Y,
                )
            } {
                0 => Ok(false),
                1 => Ok(true),
                _ => Err(Error::Custom("Button press error".to_string())),
            },
            controller_input::Digital::Up => match unsafe {
                bindings::controller_get_digital(
                    self.controller_type,
                    bindings::controller_digital_e_t_E_CONTROLLER_DIGITAL_UP,
                )
            } {
                0 => Ok(false),
                1 => Ok(true),
                _ => Err(Error::Custom("Button press error".to_string())),
            },
            controller_input::Digital::Down => match unsafe {
                bindings::controller_get_digital(
                    self.controller_type,
                    bindings::controller_digital_e_t_E_CONTROLLER_DIGITAL_DOWN,
                )
            } {
                0 => Ok(false),
                1 => Ok(true),
                _ => Err(Error::Custom("Button press error".to_string())),
            },
            controller_input::Digital::Left => match unsafe {
                bindings::controller_get_digital(
                    self.controller_type,
                    bindings::controller_digital_e_t_E_CONTROLLER_DIGITAL_LEFT,
                )
            } {
                0 => Ok(false),
                1 => Ok(true),
                _ => Err(Error::Custom("Button press error".to_string())),
            },
            controller_input::Digital::Right => match unsafe {
                bindings::controller_get_digital(
                    self.controller_type,
                    bindings::controller_digital_e_t_E_CONTROLLER_DIGITAL_RIGHT,
                )
            } {
                0 => Ok(false),
                1 => Ok(true),
                _ => Err(Error::Custom("Button press error".to_string())),
            },
            controller_input::Digital::L1 => match unsafe {
                bindings::controller_get_digital(
                    self.controller_type,
                    bindings::controller_digital_e_t_E_CONTROLLER_DIGITAL_L1,
                )
            } {
                0 => Ok(false),
                1 => Ok(true),
                _ => Err(Error::Custom("Button press error".to_string())),
            },
            controller_input::Digital::L2 => match unsafe {
                bindings::controller_get_digital(
                    self.controller_type,
                    bindings::controller_digital_e_t_E_CONTROLLER_DIGITAL_L2,
                )
            } {
                0 => Ok(false),
                1 => Ok(true),
                _ => Err(Error::Custom("Button press error".to_string())),
            },
            controller_input::Digital::R1 => match unsafe {
                bindings::controller_get_digital(
                    self.controller_type,
                    bindings::controller_digital_e_t_E_CONTROLLER_DIGITAL_R1,
                )
            } {
                0 => Ok(false),
                1 => Ok(true),
                _ => Err(Error::Custom("Button press error".to_string())),
            },
            controller_input::Digital::R2 => match unsafe {
                bindings::controller_get_digital(
                    self.controller_type,
                    bindings::controller_digital_e_t_E_CONTROLLER_DIGITAL_R2,
                )
            } {
                0 => Ok(false),
                1 => Ok(true),
                _ => Err(Error::Custom("Button press error".to_string())),
            },
        }
    }

    /// Reads a value from a controller analog joystick
    #[allow(dead_code)]
    pub fn get_analog(&self, axis: controller_input::Analog) -> Result<f64, Error> {
        match axis {
            controller_input::Analog::LeftX => match unsafe {
                bindings::controller_get_analog(
                    self.controller_type,
                    bindings::controller_analog_e_t_E_CONTROLLER_ANALOG_LEFT_X,
                )
            } {
                bindings::PROS_ERR_ => {
                    Err(Error::Custom("Error getting controller analog".to_string()))
                }
                x => Ok(f64::from(x) / 127.),
            },
            controller_input::Analog::LeftY => match unsafe {
                bindings::controller_get_analog(
                    self.controller_type,
                    bindings::controller_analog_e_t_E_CONTROLLER_ANALOG_LEFT_Y,
                )
            } {
                bindings::PROS_ERR_ => {
                    Err(Error::Custom("Error getting controller analog".to_string()))
                }
                x => Ok(f64::from(x) / 127.),
            },
            controller_input::Analog::RightX => match unsafe {
                bindings::controller_get_analog(
                    self.controller_type,
                    bindings::controller_analog_e_t_E_CONTROLLER_ANALOG_RIGHT_X,
                )
            } {
                bindings::PROS_ERR_ => {
                    Err(Error::Custom("Error getting controller analog".to_string()))
                }
                x => Ok(f64::from(x) / 127.),
            },
            controller_input::Analog::RightY => match unsafe {
                bindings::controller_get_analog(
                    self.controller_type,
                    bindings::controller_analog_e_t_E_CONTROLLER_ANALOG_RIGHT_Y,
                )
            } {
                bindings::PROS_ERR_ => {
                    Err(Error::Custom("Error getting controller analog".to_string()))
                }
                x => Ok(f64::from(x) / 127.),
            },
        }
    }
}
