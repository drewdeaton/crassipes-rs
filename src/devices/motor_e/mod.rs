use crate::units::unit_constants::*;
use crate::units::*;
use alloc::vec;
use alloc::vec::Vec;
use crs_bind::bindings;
use crs_bind::error::Error;
use crate::devices::motor::motor_settings::{BrakeMode, Gearset};

#[derive(Clone)]
pub enum Motor {
    SmartMotor(u8, i32, Gearset),
    MotorGroup(Vec<(u8, i32, Gearset)>),
}

impl Motor {
    //Movement functions

    /// Moves the motor to an absolute rotation in relation to where it was last
    /// tared
    // The motor functions should generally follow this template
    pub fn move_absolute(&self, i: QAngle) -> Result<(), Error> {
        match self {
            Motor::SmartMotor(port, reversed, gear_set) => {
                match unsafe {
                    bindings::motor_move_absolute(
                        *port,
                        (*reversed as f64) * i.convert(DEGREE) * (gear_set.to_ratio()),
                        200,
                    )
                } {
                    bindings::PROS_ERR_ => Err(Error::Custom(
                        "Error moving motor to an absolute position".into(),
                    )),
                    _ => Ok(()),
                }
            }
            Motor::MotorGroup(motors) => motors
                .iter()
                .map(|(port, reversed, gear_set)| {
                    match unsafe {
                        bindings::motor_move_absolute(
                            *port,
                            *reversed as f64 * i.convert(DEGREE) * (gear_set.to_ratio()),
                            200,
                        )
                    } {
                        bindings::PROS_ERR_ => Err(Error::Custom(
                            "Error moving motor to an absolute position".into(),
                        )),
                        _ => Ok(()),
                    }
                })
                .collect::<Result<(), Error>>(),
            _ => Err(Error::Custom("Unknown motor type".into())),
        }
    }
    /// Moves the motor relative to its current position
    pub fn move_relative(&self, i: QAngle) -> Result<(), Error> {
        match self {
            Motor::SmartMotor(port, reversed, gear_set) => {
                match unsafe {
                    bindings::motor_move_relative(
                        *port,
                        (*reversed as f64) * i.convert(DEGREE) * (gear_set.to_ratio()),
                        200,
                    )
                } {
                    bindings::PROS_ERR_ => Err(Error::Custom(
                        "Error moving motor to a relative position".into(),
                    )),
                    _ => Ok(()),
                }
            }
            Motor::MotorGroup(motors) => motors
                .iter()
                .map(|(port, reversed, gear_set)| {
                    match unsafe {
                        bindings::motor_move_relative(
                            *port,
                            *reversed as f64 * i.convert(DEGREE) * (gear_set.to_ratio()),
                            200,
                        )
                    } {
                        bindings::PROS_ERR_ => Err(Error::Custom(
                            "Error moving motor to a relative position".into(),
                        )),
                        _ => Ok(()),
                    }
                })
                .collect::<Result<(), Error>>(),
            _ => Err(Error::Custom("Unknown motor type".into())),
        }
    }

    /// Moves the motor at a specified velocity
    pub fn move_velocity(&self, i: f64) -> Result<(), Error> {
        match self {
            Motor::SmartMotor(port, reversed, gear_set) => {
                match unsafe {
                    bindings::motor_move_velocity(
                        *port,
                        ((*reversed as f64) * i * (gear_set.to_ratio())) as i32,
                    )
                } {
                    bindings::PROS_ERR_ => {
                        Err(Error::Custom("Error moving motor at a velocity".into()))
                    }
                    _ => Ok(()),
                }
            }
            Motor::MotorGroup(motors) => motors
                .iter()
                .map(|(port, reversed, gear_set)| {
                    match unsafe {
                        bindings::motor_move_velocity(
                            *port,
                            (*reversed as f64 * i * (gear_set.to_ratio())) as i32,
                        )
                    } {
                        bindings::PROS_ERR_ => {
                            Err(Error::Custom("Error moving motor at a velocity".into()))
                        }
                        _ => Ok(()),
                    }
                })
                .collect::<Result<(), Error>>(),
            _ => Err(Error::Custom("Unknown motor type".into())),
        }
    }

    /// Applies a specified voltage to the motor
    pub fn move_voltage(&self, i: QVoltage) -> Result<(), Error> {
        match self {
            Motor::SmartMotor(port, reversed, gear_set) => {
                match unsafe {
                    bindings::motor_move_voltage(
                        *port,
                        ((*reversed as f64) * i.convert(VOLT / 1000.)) as i32,
                    )
                } {
                    bindings::PROS_ERR_ => {
                        Err(Error::Custom("Error moving motor at a voltage".into()))
                    }
                    _ => Ok(()),
                }
            }
            Motor::MotorGroup(motors) => motors
                .iter()
                .map(|(port, reversed, gear_set)| {
                    match unsafe {
                        bindings::motor_move_voltage(
                            *port,
                            (*reversed as f64 * i.convert(VOLT / 1000.)) as i32,
                        )
                    } {
                        bindings::PROS_ERR_ => {
                            Err(Error::Custom("Error moving motor at a voltage".into()))
                        }
                        _ => Ok(()),
                    }
                })
                .collect::<Result<(), Error>>(),
            _ => Err(Error::Custom("Unknown motor type".into())),
        }
    }

    /// Move the motor using the controller map [-1.0,1.0]
    pub fn move_cmap(&self, i: f64) -> Result<(), Error> {
        match self {
            Motor::SmartMotor(port, reversed, _) => {
                match unsafe {
                    bindings::motor_move_voltage(*port, (i * 12000.) as i32 * *reversed)
                } {
                    bindings::PROS_ERR_ => Err(Error::Custom(
                        "Error moving motor to an absolute position".into(),
                    )),
                    _ => Ok(()),
                }
            }
            Motor::MotorGroup(motors) => motors
                .iter()
                .map(|(port, reversed, _)| {
                    match unsafe {
                        bindings::motor_move_voltage(*port, (i * 12000.) as i32 * *reversed)
                    } {
                        bindings::PROS_ERR_ => Err(Error::Custom(
                            "Error moving motor to an absolute position".into(),
                        )),
                        _ => Ok(()),
                    }
                })
                .collect::<Result<(), Error>>(),
            _ => Err(Error::Custom("Unknown motor type".into())),
        }
    }

    /// Changes the output velocity for a profiled movement (moveAbsolute or
    /// moveRelative)
    pub fn modify_profiled_velocity(&self, i: i32) -> Result<(), Error> {
        match self {
            Motor::SmartMotor(port, reversed, _) => {
                match unsafe { bindings::motor_modify_profiled_velocity(*port, i) } {
                    bindings::PROS_ERR_ => Err(Error::Custom(
                        "Error moving motor to an absolute position".into(),
                    )),
                    _ => Ok(()),
                }
            }
            Motor::MotorGroup(motors) => motors
                .iter()
                .map(|(port, reversed, _)| {
                    match unsafe { bindings::motor_modify_profiled_velocity(*port, i) } {
                        bindings::PROS_ERR_ => Err(Error::Custom(
                            "Error moving motor to an absolute position".into(),
                        )),
                        _ => Ok(()),
                    }
                })
                .collect::<Result<(), Error>>(),
            _ => Err(Error::Custom("Unknown motor type".into())),
        }
    }

    //Telemetry functions
    /// Gets the position of the motor
    pub fn get_position(&self) -> Result<QAngle, Error> {
        let (port, reversed) = match self {
            Motor::SmartMotor(port, reversed, _) => (*port, *reversed),
            Motor::MotorGroup(motors) => {
                if motors.len() < 1 {
                    return Err(Error::Custom("Motor Group is empty".into()));
                }
                let (port, reversed, _) = motors[0];
                (port, reversed)
            }
        };
        match unsafe { bindings::motor_get_position(port) } {
            x if x == bindings::PROS_ERR_F_ => Err(Error::Custom("Error getting position".into())),
            x => Ok(x * reversed as f64 * DEGREE),
        }
    }

    /// Get target position
    pub fn get_target_position(&self) -> Result<QAngle, Error> {
        let (port, reversed) = match self {
            Motor::SmartMotor(port, reversed, _) => (*port, *reversed),
            Motor::MotorGroup(motors) => {
                if motors.len() < 1 {
                    return Err(Error::Custom("Motor Group is empty".into()));
                }
                let (port, reversed, _) = motors[0];
                (port, reversed)
            }
        };
        match unsafe { bindings::motor_get_target_position(port) } {
            x if x == bindings::PROS_ERR_F_ => Err(Error::Custom("Error getting position".into())),
            x => Ok(x * reversed as f64 * DEGREE),
        }
    }

    /// Gets the position error (Difference between target and current position)
    pub fn get_position_error(&self) -> Result<QAngle, Error> {
        Ok(self.get_target_position()? - self.get_position()?)
    }

    /// Resets the motor encoder so the current position is zero
    pub fn tare_position(&self) -> Result<(), Error> {
        match self {
            Motor::SmartMotor(port, reversed, _) => {
                match unsafe { bindings::motor_tare_position(*port) } {
                    bindings::PROS_ERR_ => Err(Error::Custom(
                        "Error resetting motor position".into(),
                    )),
                    _ => Ok(()),
                }
            }
            Motor::MotorGroup(motors) => motors
                .iter()
                .map(|(port, reversed, _)| {
                    match unsafe { bindings::motor_tare_position(*port) } {
                        bindings::PROS_ERR_ => Err(Error::Custom(
                            "Error resetting position".into(),
                        )),
                        _ => Ok(()),
                    }
                })
                .collect::<Result<(), Error>>(),
            _ => Err(Error::Custom("Unknown motor type".into())),
        }
    }

    /// Gets the motor target velocity
    pub fn get_target_velocity(&self) -> Result<i32, Error> {
        let (port, reversed) = match self {
            Motor::SmartMotor(port, reversed, _) => (*port, *reversed),
            Motor::MotorGroup(motors) => {
                if motors.len() < 1 {
                    return Err(Error::Custom("Motor Group is empty".into()));
                }
                let (port, reversed, _) = motors[0];
                (port, reversed)
            }
        };
        match unsafe { bindings::motor_get_target_velocity(port) } {
            x if x == bindings::PROS_ERR_ => Err(Error::Custom("Error getting position".into())),
            x => Ok(x * reversed),
        }
    }

    /// Gets the motors current velocity
    pub fn get_actual_velocity(&self) -> Result<i32, Error> {
        let (port, reversed) = match self {
            Motor::SmartMotor(port, reversed, _) => (*port, *reversed),
            Motor::MotorGroup(motors) => {
                if motors.len() < 1 {
                    return Err(Error::Custom("Motor Group is empty".into()));
                }
                let (port, reversed, _) = motors[0];
                (port, reversed)
            }
        };
        match unsafe { bindings::motor_get_actual_velocity(port) } {
            x if x == bindings::PROS_ERR_F_ => Err(Error::Custom("Error getting position".into())),
            x => Ok(x as i32 * reversed),
        }
    }

    /// Gets the difference between the target velocity and the actual velocity
    pub fn get_velocity_error(&self) -> Result<i32, Error> {
        Ok(self.get_target_velocity()? - self.get_actual_velocity()?)
    }

    /// Set brake mode
    pub fn set_brake_mode(&self, brake_mode: BrakeMode) -> Result<(), Error> {
        todo!()
    }

    /// Get brake mode
    pub fn get_brake_mode(&self) -> Result<BrakeMode, Error> {
        todo!()
    }

    /// Get gearing
    pub fn get_gearing(&self) -> Result<Gearset, Error> {
        match self {
            Motor::SmartMotor(_, _, g) => Ok(*g),
            Motor::MotorGroup(motors) => {
                if motors.len() < 1 {
                    return Err(Error::Custom("Motor Group is empty".into()));
                }
                Ok(motors[1].2)
            },
        }
    }
}

impl core::ops::Add<Motor> for Motor {
    type Output = Self;

    fn add(self, rhs: Motor) -> Self::Output {
        match (self, rhs) {
            // Case for addition of 2 SmartMotor variants to produce one MotorGroup variant
            (
                Motor::SmartMotor(lport, lreversed, lgearset),
                Motor::SmartMotor(rport, rreversed, rgearset),
            ) => Motor::MotorGroup(vec![
                (lport, lreversed, lgearset),
                (rport, rreversed, rgearset),
            ]),
            // Case for addition of 2 MotorGroup variants to produce one MotorGroup variant
            (Motor::MotorGroup(l), Motor::MotorGroup(r)) => Motor::MotorGroup(
                l.iter()
                    .chain(r.iter())
                    .cloned()
                    .collect::<Vec<(u8, i32, Gearset)>>(),
            ),
            // Case for addition of a MotorGroup and a SmartMotor variant to produce one MotorGroup
            (Motor::MotorGroup(gp), Motor::SmartMotor(a_port, a_rev, a_gear)) => Motor::MotorGroup(
                gp.iter()
                    .chain(vec![(a_port, a_rev, a_gear)].iter())
                    .cloned()
                    .collect::<Vec<(u8, i32, Gearset)>>(),
            ),
            // Case for addition of a SmartMotor and a MotorGroup variant to produce one MotorGroup
            (Motor::SmartMotor(a_port, a_rev, a_gear), Motor::MotorGroup(gp)) => Motor::MotorGroup(
                gp.iter()
                    .chain(vec![(a_port, a_rev, a_gear)].iter())
                    .cloned()
                    .collect::<Vec<(u8, i32, Gearset)>>(),
            ),
        }
    }
}
