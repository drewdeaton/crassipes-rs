use super::*;

use crate::units::{QAngle, QCurrent, QHeat, QPower, QTorque, QVoltage};
use alloc::boxed::Box;
use alloc::vec::Vec;
use crs_bind::error::Error;

pub struct MotorGroup {
    motors: Vec<Box<dyn AbstractMotor>>,
}

impl MotorGroup {
    #[allow(dead_code)]
    pub fn new(k: Vec<Box<dyn AbstractMotor>>) -> Self {
        Self { motors: k }
    }
}

impl AbstractMotor for MotorGroup {
    //Movement functions
    fn move_absolute(&self, i: QAngle) -> Result<(), Error> {
        for el in self.motors.iter() {
            (*el).move_absolute(i).unwrap_or(());
        }

        Ok(())
    }
    fn move_relative(&self, i: QAngle) -> Result<(), Error> {
        for el in self.motors.iter() {
            (*el).move_relative(i).unwrap_or(());
        }

        Ok(())
    }
    fn move_velocity(&self, i: f64) -> Result<(), Error> {
        for el in self.motors.iter() {
            (*el).move_velocity(i).unwrap_or(());
        }

        Ok(())
    }
    fn move_voltage(&self, i: QVoltage) -> Result<(), Error> {
        for el in self.motors.iter() {
            (*el).move_voltage(i).unwrap_or(());
        }

        Ok(())
    }
    fn modify_profiled_velocity(&self, i: f64) -> Result<(), Error> {
        for el in self.motors.iter() {
            (*el).modify_profiled_velocity(i).unwrap_or(());
        }

        Ok(())
    }

    //Telemetry functions
    fn get_position(&self) -> Result<QAngle, Error> {
        self.motors[0].get_position()
    }
    fn get_target_position(&self) -> Result<QAngle, Error> {
        self.motors[0].get_target_position()
    }
    fn get_position_error(&self) -> Result<QAngle, Error> {
        self.motors[0].get_position_error()
    }
    fn tare_position(&self) -> Result<(), Error> {
        for el in self.motors.iter() {
            (*el).tare_position().unwrap_or(());
        }

        Ok(())
    }
    fn get_target_velocity(&self) -> Result<i32, Error> {
        self.motors[0].get_target_velocity()
    }
    fn get_actual_velocity(&self) -> Result<i32, Error> {
        self.motors[0].get_actual_velocity()
    }
    fn get_velocity_error(&self) -> Result<i32, Error> {
        self.motors[0].get_velocity_error()
    }
    fn get_current_draw(&self) -> Result<QCurrent, Error> {
        let mut ret: QCurrent = QCurrent::new(0.);
        for el in self.motors.iter() {
            ret = ret + (*el).get_current_draw().unwrap_or(QCurrent::new(0.0));
        }
        Ok(ret / self.motors.len() as f64)
    }
    fn get_direction(&self) -> Result<i32, Error> {
        self.motors[0].get_direction()
    }
    fn get_efficiency(&self) -> Result<f64, Error> {
        self.motors[0].get_efficiency()
    }
    fn is_over_current(&self) -> Result<bool, Error> {
        for el in self.motors.iter() {
            match (*el).is_over_current().unwrap_or(true) {
                true => {
                    return Ok(true);
                }
                false => continue,
            }
        }

        Ok(false)
    }
    fn is_over_temp(&self) -> Result<bool, Error> {
        for el in self.motors.iter() {
            match (*el).is_over_temp().unwrap_or(false) {
                true => {
                    return Ok(true);
                }
                false => continue,
            }
        }

        Ok(false)
    }
    fn get_power(&self) -> Result<QPower, Error> {
        let mut ret: QPower = QPower::new(0.);
        for el in self.motors.iter() {
            ret = ret + (*el).get_power().unwrap_or(QPower::new(0.0));
        }
        Ok(ret / self.motors.len() as f64)
    }
    fn get_temperature(&self) -> Result<QHeat, Error> {
        let mut ret: QHeat = QHeat::new(0.);
        for el in self.motors.iter() {
            ret = ret + (*el).get_temperature().unwrap_or(QHeat::new(0.0));
        }
        Ok(ret / self.motors.len() as f64)
    }
    fn get_torque(&self) -> Result<QTorque, Error> {
        self.motors[0].get_torque()
    }
    fn get_voltage(&self) -> Result<QVoltage, Error> {
        self.motors[0].get_voltage()
    }
    fn set_brake_mode(&self, brake_mode: motor_settings::BrakeMode) -> Result<(), Error> {
        for el in self.motors.iter() {
            (*el).set_brake_mode(brake_mode).unwrap();
        }

        Ok(())
    }
    fn get_brake_mode(&self) -> Result<motor_settings::BrakeMode, Error> {
        self.motors[0].get_brake_mode()
    }
    fn set_current_limit(&self, limit: QCurrent) -> Result<(), Error> {
        for el in self.motors.iter() {
            (*el).set_current_limit(limit).unwrap();
        }

        Ok(())
    }
    fn get_current_limit(&self) -> Result<QCurrent, Error> {
        self.motors[0].get_current_limit()
    }
    fn set_gearing(&self, gearset: motor_settings::Gearset) -> Result<(), Error> {
        for el in self.motors.iter() {
            (*el).set_gearing(gearset).unwrap();
        }

        Ok(())
    }
    fn get_gearing(&self) -> Result<motor_settings::Gearset, Error> {
        self.motors[0].get_gearing()
    }
    fn set_reversed(&self, reversed: bool) -> Result<(), Error> {
        for el in self.motors.iter() {
            (*el).set_reversed(reversed).unwrap();
        }

        Ok(())
    }
    fn set_voltage_limit(&self, voltage: QVoltage) -> Result<(), Error> {
        for el in self.motors.iter() {
            (*el).set_voltage_limit(voltage).unwrap();
        }

        Ok(())
    }
}
