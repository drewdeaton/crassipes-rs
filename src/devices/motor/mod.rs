mod motor_group;
mod smart_motor;
use crate::units::*;
pub use motor_group::*;
pub use smart_motor::*;

use crs_bind::error::Error;

pub trait AbstractMotor {
    //Movement functions

    /// Moves the motor to an absolute rotation in relation to where it was last
    /// tared
    fn move_absolute(&self, i: QAngle) -> Result<(), Error>;

    /// Moves the motor relative to its current position
    fn move_relative(&self, i: QAngle) -> Result<(), Error>;

    /// Moves the motor at a specified velocity
    fn move_velocity(&self, i: f64) -> Result<(), Error>;

    /// Applies a specified voltage to the motor
    fn move_voltage(&self, i: QVoltage) -> Result<(), Error>;

    /// Changes the output velocity for a profiled movement (moveAbsolute or
    /// moveRelative)
    fn modify_profiled_velocity(&self, i: f64) -> Result<(), Error>;

    //Telemetry functions
    /// Gets the position of the robot
    fn get_position(&self) -> Result<QAngle, Error>;

    /// Get target position
    fn get_target_position(&self) -> Result<QAngle, Error>;

    /// Gets the position error (Difference between target and current position)
    fn get_position_error(&self) -> Result<QAngle, Error>;

    /// Resets the motor encoder so the current position is zero
    fn tare_position(&self) -> Result<(), Error>;

    /// Gets the motor target velocity
    fn get_target_velocity(&self) -> Result<i32, Error>;

    /// Gets the motors current velocity
    fn get_actual_velocity(&self) -> Result<i32, Error>;

    /// Gets the difference between the target velocity and the actual velocity
    fn get_velocity_error(&self) -> Result<i32, Error>;

    /// Gets the motor current draw
    fn get_current_draw(&self) -> Result<QCurrent, Error>;

    /// Gets the direction of movement (1 for forward, -1 for reverse)
    fn get_direction(&self) -> Result<i32, Error>;

    /// Gets the efficiency of the motor from 0 to 1
    fn get_efficiency(&self) -> Result<f64, Error>;

    /// Checks if the motor is over its current limit
    fn is_over_current(&self) -> Result<bool, Error>;

    /// Checks if the motor is over its temperature limit
    fn is_over_temp(&self) -> Result<bool, Error>;

    /// Get power drawn by the motor
    fn get_power(&self) -> Result<QPower, Error>;

    /// Get the motor temperature
    fn get_temperature(&self) -> Result<QHeat, Error>;

    /// Get motor torque
    fn get_torque(&self) -> Result<QTorque, Error>;

    /// Get motor voltage
    fn get_voltage(&self) -> Result<QVoltage, Error>;

    /// Set brake mode
    fn set_brake_mode(&self, brake_mode: motor_settings::BrakeMode) -> Result<(), Error>;

    /// Get brake mode
    fn get_brake_mode(&self) -> Result<motor_settings::BrakeMode, Error>;

    /// Set current limit
    fn set_current_limit(&self, limit: QCurrent) -> Result<(), Error>;

    /// Get current limit
    fn get_current_limit(&self) -> Result<QCurrent, Error>;

    /// Set gearing
    fn set_gearing(&self, gearset: motor_settings::Gearset) -> Result<(), Error>;

    /// Get gearing
    fn get_gearing(&self) -> Result<motor_settings::Gearset, Error>;

    /// Set reversed
    fn set_reversed(&self, reversed: bool) -> Result<(), Error>;

    /// Set voltage limit
    fn set_voltage_limit(&self, voltage: QVoltage) -> Result<(), Error>;
}

pub mod motor_settings {
    #[derive(Clone, Copy)]
    pub enum Gearset {
        /// Blue 6:1 Gearset
        Blue,
        /// Green 18:1 Gearset
        Green,
        /// Red 36:1 Gearset
        Red,
        /// Invalid gearset (please don't try to use this)
        Invalid,
    }
    #[derive(Clone, Copy)]
    pub enum BrakeMode {
        /// Coast
        Coast,
        /// Hold
        Hold,
        /// Brake
        Brake,
        /// Invalid brake mode (please don't try to use this)
        Invalid,
    }

    impl Gearset {
        #[inline]
        pub fn to_ratio(self) -> f64 {
            match self {
                Gearset::Blue => 1. / 3.,
                Gearset::Red => 2.,
                Gearset::Green => 1.,
                _ => unreachable!(),
            }
        }
    }
}
