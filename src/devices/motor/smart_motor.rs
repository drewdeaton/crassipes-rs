use super::{motor_settings, AbstractMotor};

use crate::units::{
    unit_constants::{AMPERE, DEGREE, KELVIN, WATT},
    QAngle, QCurrent, QHeat, QPower, QTorque, QVoltage,
};
use alloc::string::ToString;
use crs_bind::bindings;
use crs_bind::error::Error;

pub struct SmartMotor {
    port: u8,
}

impl SmartMotor {
    #[allow(dead_code)]
    pub fn new(iport: i8, i_gearset: motor_settings::Gearset) -> Self {
        let motor = Self {
            port: iport.abs() as u8,
        };

        motor.set_encoder_units().unwrap();
        motor.set_gearing(i_gearset).unwrap();
        motor.set_reversed(iport < 0).unwrap();

        motor
    }

    pub fn set_encoder_units(&self) -> Result<(), Error> {
        match unsafe {
            bindings::motor_set_encoder_units(
                self.port,
                bindings::motor_encoder_units_e_E_MOTOR_ENCODER_DEGREES,
            )
        } {
            bindings::PROS_ERR_ => Err(Error::Custom("Error".to_string())),
            _ => Ok(()),
        }
    } // fn set_encoder_units(&self, i_encoder_units : motor_settings::EncoderUnits)
      // -> Result<(), Error> {
}

impl AbstractMotor for SmartMotor {
    /// Move an absolute amount
    fn move_absolute(&self, i: QAngle) -> Result<(), Error> {
        match unsafe { bindings::motor_move_absolute(self.port, i.convert(DEGREE), 200) } {
            bindings::PROS_ERR_ => Err(Error::Custom("Error".to_string())),
            _ => Ok(()),
        }
    }
    fn move_relative(&self, i: QAngle) -> Result<(), Error> {
        match unsafe { bindings::motor_move_relative(self.port, i.convert(DEGREE), 200) } {
            bindings::PROS_ERR_ => Err(Error::Custom("Error".to_string())),
            _ => Ok(()),
        }
    }
    fn move_velocity(&self, i: f64) -> Result<(), Error> {
        match unsafe { bindings::motor_move_velocity(self.port, i as i32) } {
            bindings::PROS_ERR_ => Err(Error::Custom("Error".to_string())),
            _ => Ok(()),
        }
    }

    fn modify_profiled_velocity(&self, i: f64) -> Result<(), Error> {
        match unsafe { bindings::motor_modify_profiled_velocity(self.port, i as i32) } {
            bindings::PROS_ERR_ => Err(Error::Custom(
                "Error modifying profiled velocity".to_string(),
            )),
            _ => Ok(()),
        }
    }

    fn move_voltage(&self, i: QVoltage) -> Result<(), Error> {
        match unsafe { bindings::motor_move_voltage(self.port, (i.value * 1000.) as i32) } {
            bindings::PROS_ERR_ => Err(Error::Custom("Error".to_string())),
            _ => Ok(()),
        }
    }
    //Telemetry functions
    fn get_position(&self) -> Result<QAngle, Error> {
        match unsafe { bindings::motor_get_position(self.port) } {
            x if x == bindings::PROS_ERR_F_ => {
                Err(Error::Custom("Error getting position".to_string()))
            }
            x => Ok(x * DEGREE),
        }
    }

    fn get_target_position(&self) -> Result<QAngle, Error> {
        match unsafe { bindings::motor_get_target_position(self.port) } {
            x if x == bindings::PROS_ERR_F_ => {
                Err(Error::Custom("Error getting target position".to_string()))
            }
            x => Ok(x * DEGREE),
        }
    }

    fn get_position_error(&self) -> Result<QAngle, Error> {
        Ok(self.get_target_position().unwrap() - self.get_position().unwrap())
    }

    fn tare_position(&self) -> Result<(), Error> {
        match unsafe { bindings::motor_tare_position(self.port) } {
            bindings::PROS_ERR_ => Err(Error::Custom("Could not reset motor encoder".to_string())),
            _ => Ok(()),
        }
    }

    fn get_target_velocity(&self) -> Result<i32, Error> {
        match unsafe { bindings::motor_get_target_velocity(self.port) } {
            x if x == bindings::PROS_ERR_ => {
                Err(Error::Custom("Error getting target position".to_string()))
            }
            x => Ok(x),
        }
    }

    fn get_actual_velocity(&self) -> Result<i32, Error> {
        match unsafe { bindings::motor_get_actual_velocity(self.port) } {
            x if x == bindings::PROS_ERR_F_ => {
                Err(Error::Custom("Error getting target position".to_string()))
            }
            x => Ok(x as i32),
        }
    }

    fn get_velocity_error(&self) -> Result<i32, Error> {
        Ok(self.get_target_velocity().unwrap() - self.get_actual_velocity().unwrap())
    }

    fn get_current_draw(&self) -> Result<QCurrent, Error> {
        match unsafe { bindings::motor_get_current_draw(self.port) } {
            x if x == bindings::PROS_ERR_ => {
                Err(Error::Custom("Error getting current draw".to_string()))
            }
            x => Ok(x as f64 * 0.001 * AMPERE),
        }
    }

    fn get_direction(&self) -> Result<i32, Error> {
        match unsafe { bindings::motor_get_direction(self.port) } {
            x if x == bindings::PROS_ERR_ => {
                Err(Error::Custom("Error getting direction".to_string()))
            }
            x => Ok(x),
        }
    }

    fn get_efficiency(&self) -> Result<f64, Error> {
        match unsafe { bindings::motor_get_efficiency(self.port) } {
            x if x == bindings::PROS_ERR_F_ => {
                Err(Error::Custom("Error getting efficiency".to_string()))
            }
            x => Ok(x),
        }
    }

    fn is_over_current(&self) -> Result<bool, Error> {
        match unsafe { bindings::motor_is_over_current(self.port) } {
            x if x == bindings::PROS_ERR_ => {
                Err(Error::Custom("Error getting is over current".to_string()))
            }
            x => match x {
                0 => Ok(false),
                _ => Ok(true),
            },
        }
    }

    fn is_over_temp(&self) -> Result<bool, Error> {
        match unsafe { bindings::motor_is_over_temp(self.port) } {
            x if x == bindings::PROS_ERR_ => {
                Err(Error::Custom("Error getting is over temp".to_string()))
            }
            x => match x {
                0 => Ok(false),
                _ => Ok(true),
            },
        }
    }
    fn get_power(&self) -> Result<QPower, Error> {
        match unsafe { bindings::motor_get_power(self.port) } {
            x if x == bindings::PROS_ERR_F_ => {
                Err(Error::Custom("Error getting drawn power".to_string()))
            }
            x => Ok(x * WATT),
        }
    }

    fn get_temperature(&self) -> Result<QHeat, Error> {
        match unsafe { bindings::motor_get_temperature(self.port) } {
            x if x == bindings::PROS_ERR_F_ => {
                Err(Error::Custom("Error getting temperature".to_string()))
            }
            x => Ok(x * KELVIN + QHeat { value: 273.15 }),
        }
    }

    fn get_torque(&self) -> Result<QTorque, Error> {
        match unsafe { bindings::motor_get_torque(self.port) } {
            x if x == bindings::PROS_ERR_F_ => {
                Err(Error::Custom("Error getting torque".to_string()))
            }
            x => Ok(QTorque { value: x }),
        }
    }

    fn get_voltage(&self) -> Result<QVoltage, Error> {
        match unsafe { bindings::motor_get_voltage(self.port) } {
            x if x == bindings::PROS_ERR_ => {
                Err(Error::Custom("Error getting voltage".to_string()))
            }
            x => Ok(QVoltage {
                value: x as f64 / 1000.,
            }),
        }
    }

    fn set_brake_mode(&self, brake_mode: motor_settings::BrakeMode) -> Result<(), Error> {
        match unsafe {
            bindings::motor_set_brake_mode(
                self.port,
                match brake_mode {
                    motor_settings::BrakeMode::Brake => {
                        bindings::motor_brake_mode_e_E_MOTOR_BRAKE_BRAKE
                    }
                    motor_settings::BrakeMode::Hold => {
                        bindings::motor_brake_mode_e_E_MOTOR_BRAKE_HOLD
                    }
                    motor_settings::BrakeMode::Coast => {
                        bindings::motor_brake_mode_e_E_MOTOR_BRAKE_COAST
                    }
                    motor_settings::BrakeMode::Invalid => {
                        bindings::motor_brake_mode_e_E_MOTOR_BRAKE_INVALID
                    }
                },
            )
        } {
            bindings::PROS_ERR_ => Err(Error::Custom("Error setting brake mode".to_string())),
            _ => Ok(()),
        }
    }

    fn get_brake_mode(&self) -> Result<motor_settings::BrakeMode, Error> {
        match unsafe { bindings::motor_get_brake_mode(self.port) } {
            bindings::motor_brake_mode_e_E_MOTOR_BRAKE_BRAKE => {
                Ok(motor_settings::BrakeMode::Brake)
            }
            bindings::motor_brake_mode_e_E_MOTOR_BRAKE_HOLD => Ok(motor_settings::BrakeMode::Hold),
            bindings::motor_brake_mode_e_E_MOTOR_BRAKE_COAST => {
                Ok(motor_settings::BrakeMode::Coast)
            }
            _ => Ok(motor_settings::BrakeMode::Invalid),
        }
    }

    fn set_current_limit(&self, limit: QCurrent) -> Result<(), Error> {
        match unsafe {
            bindings::motor_set_current_limit(
                self.port,
                limit.convert(QCurrent { value: 0.001 }) as i32,
            )
        } {
            bindings::PROS_ERR_ => Err(Error::Custom("Error setting current limit".to_string())),
            _ => Ok(()),
        }
    }

    fn get_current_limit(&self) -> Result<QCurrent, Error> {
        match unsafe { bindings::motor_get_current_limit(self.port) } {
            x if x == bindings::PROS_ERR_ => {
                Err(Error::Custom("Error getting current limit".to_string()))
            }
            x => Ok(x as f64 * QCurrent { value: 0.001 }),
        }
    }

    fn set_gearing(&self, i_gearset: motor_settings::Gearset) -> Result<(), Error> {
        match unsafe {
            match i_gearset {
                motor_settings::Gearset::Blue => bindings::motor_set_gearing(
                    self.port,
                    bindings::motor_gearset_e_E_MOTOR_GEARSET_06,
                ),
                motor_settings::Gearset::Red => bindings::motor_set_gearing(
                    self.port,
                    bindings::motor_gearset_e_E_MOTOR_GEARSET_36,
                ),
                motor_settings::Gearset::Green => bindings::motor_set_gearing(
                    self.port,
                    bindings::motor_gearset_e_E_MOTOR_GEARSET_18,
                ),
                _ => bindings::motor_set_gearing(
                    self.port,
                    bindings::motor_gearset_e_E_MOTOR_GEARSET_INVALID,
                ),
            }
        } {
            bindings::PROS_ERR_ => Err(Error::Custom("Error".to_string())),
            _ => Ok(()),
        }
    }

    fn get_gearing(&self) -> Result<motor_settings::Gearset, Error> {
        match unsafe { bindings::motor_get_gearing(self.port) } {
            bindings::motor_gearset_e_E_MOTOR_GEARSET_06 => Ok(motor_settings::Gearset::Blue),
            bindings::motor_gearset_e_E_MOTOR_GEARSET_18 => Ok(motor_settings::Gearset::Green),
            bindings::motor_gearset_e_E_MOTOR_GEARSET_36 => Ok(motor_settings::Gearset::Red),
            _ => Ok(motor_settings::Gearset::Invalid),
        }
    }

    fn set_reversed(&self, reversed: bool) -> Result<(), Error> {
        match unsafe { bindings::motor_set_reversed(self.port, reversed) } {
            bindings::PROS_ERR_ => Err(Error::Custom("Error setting reversed".to_string())),
            _ => Ok(()),
        }
    }

    fn set_voltage_limit(&self, voltage: QVoltage) -> Result<(), Error> {
        match unsafe {
            bindings::motor_set_voltage_limit(
                self.port,
                voltage.convert(QVoltage { value: 0.001 }) as i32,
            )
        } {
            bindings::PROS_ERR_ => Err(Error::Custom("Error setting voltage limit".to_string())),
            _ => Ok(()),
        }
    }
}
