/// Integrated motor encoder
pub struct IntegratedEncoder {
    port: i8,
    reversed: bool,
}

impl IntegratedEncoder {
    pub fn new(iport: u8) -> Self {
        Self { port: iport }
    }

    fn controller_get(&self) -> Result<QAngle, Error> {
        self.get()
    }
}

impl ContinuousRotarySensor for IntegratedEncoder {
    fn get(&self) -> Result<QAngle, Error> {
        match unsafe { bindings::motor_get_position(self.port) } {
            x if x == bindings::PROS_ERR_F_ => {
                Err(Error::Custom("Motor encoder error".to_string()))
            }
            x => Ok(x * DEGREE),
        }
    }
}

impl RotarySensor for IntegratedEncoder {
    fn reset(&self) -> Result<(), Error> {
        match unsafe { bindings::motor_tare_position(self.port) } {
            PROS_ERR_ => Err(Error::Custom("Could not reset motor encoder".to_string())),
            _ => Ok(()),
        }
    }
}
