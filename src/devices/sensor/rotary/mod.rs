pub mod integrated_encoder;
pub use integrated_encoder::IntegratedEncoder;

pub trait RotarySensor {
    /// Gets the value of the sensor
    fn get(&self) -> Result<QAngle, Error>;
}

pub trait ContinuousRotarySensor {
    /// Resets the sensor to 0
    fn reset(&self) -> Result<(), Error>;
}

impl RotarySensor for ContinuousRotarySensor {}
