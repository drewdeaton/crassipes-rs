use core::ops::*;

#[derive(Clone, Copy)]
pub struct RQuantity<
    const LEN: i64,
    const MASS: i64,
    const ANG: i64,
    const TIME: i64,
    const CURR: i64,
    const MOLE: i64,
    const LUMINOUS: i64,
    const HEAT: i64,
> {
    pub value: f64,
}

impl<
        const LEN: i64,
        const MASS: i64,
        const ANG: i64,
        const TIME: i64,
        const CURR: i64,
        const MOLE: i64,
        const LUMINOUS: i64,
        const HEAT: i64,
    > RQuantity<LEN, MASS, ANG, TIME, CURR, MOLE, LUMINOUS, HEAT>
{
    pub fn new(i: f64) -> Self {
        Self { value: i }
    }
}

impl<
        const LEN: i64,
        const MASS: i64,
        const ANG: i64,
        const TIME: i64,
        const CURR: i64,
        const MOLE: i64,
        const LUMINOUS: i64,
        const HEAT: i64,
    > Add for RQuantity<LEN, MASS, ANG, TIME, CURR, MOLE, LUMINOUS, HEAT>
{
    type Output = RQuantity<LEN, MASS, ANG, TIME, CURR, MOLE, LUMINOUS, HEAT>;
    fn add(self, rhs: Self) -> Self {
        RQuantity {
            value: self.value + rhs.value,
        }
    }
}

impl<
        const LEN: i64,
        const MASS: i64,
        const ANG: i64,
        const TIME: i64,
        const CURR: i64,
        const MOLE: i64,
        const LUMINOUS: i64,
        const HEAT: i64,
    > Sub for RQuantity<LEN, MASS, ANG, TIME, CURR, MOLE, LUMINOUS, HEAT>
{
    type Output = RQuantity<LEN, MASS, ANG, TIME, CURR, MOLE, LUMINOUS, HEAT>;
    fn sub(self, rhs: Self) -> Self {
        RQuantity {
            value: self.value - rhs.value,
        }
    }
}

#[allow(clippy::suspicious_arithmetic_impl)]
impl<
        const L1: i64,
        const L2: i64,
        const M1: i64,
        const M2: i64,
        const A1: i64,
        const A2: i64,
        const T1: i64,
        const T2: i64,
        const C1: i64,
        const C2: i64,
        const H1: i64,
        const H2: i64,
        const U1: i64,
        const U2: i64,
        const W1: i64,
        const W2: i64,
    > Mul<RQuantity<L2, M2, A2, T2, C2, H2, U2, W2>> for RQuantity<L1, M1, A1, T1, C1, H1, U1, W1>
where
    RQuantity<
        { L1 + L2 },
        { M1 + M2 },
        { A1 + A2 },
        { T1 + T2 },
        { C1 + C2 },
        { H1 + H2 },
        { U1 + U2 },
        { W1 + W2 },
    >: Sized,
{
    type Output = RQuantity<
        { L1 + L2 },
        { M1 + M2 },
        { A1 + A2 },
        { T1 + T2 },
        { C1 + C2 },
        { H1 + H2 },
        { U1 + U2 },
        { W1 + W2 },
    >;

    fn mul(self, rhs: RQuantity<L2, M2, A2, T2, C2, H2, U2, W2>) -> Self::Output {
        Self::Output {
            value: self.value * rhs.value,
        }
    }
}

#[allow(clippy::suspicious_arithmetic_impl)]
impl<
        const L1: i64,
        const L2: i64,
        const M1: i64,
        const M2: i64,
        const A1: i64,
        const A2: i64,
        const T1: i64,
        const T2: i64,
        const C1: i64,
        const C2: i64,
        const H1: i64,
        const H2: i64,
        const U1: i64,
        const U2: i64,
        const W1: i64,
        const W2: i64,
    > Div<RQuantity<L2, M2, A2, T2, C2, H2, U2, W2>> for RQuantity<L1, M1, A1, T1, C1, H1, U1, W1>
where
    RQuantity<
        { L1 - L2 },
        { M1 - M2 },
        { A1 - A2 },
        { T1 - T2 },
        { C1 - C2 },
        { H1 - H2 },
        { U1 - U2 },
        { W1 - W2 },
    >: Sized,
{
    type Output = RQuantity<
        { L1 - L2 },
        { M1 - M2 },
        { A1 - A2 },
        { T1 - T2 },
        { C1 - C2 },
        { H1 - H2 },
        { U1 - U2 },
        { W1 - W2 },
    >;

    fn div(self, rhs: RQuantity<L2, M2, A2, T2, C2, H2, U2, W2>) -> Self::Output {
        Self::Output {
            value: self.value / rhs.value,
        }
    }
}

impl<
        const LEN: i64,
        const MASS: i64,
        const ANG: i64,
        const TIME: i64,
        const CURR: i64,
        const MOLE: i64,
        const LUMINOUS: i64,
        const HEAT: i64,
    > Mul<RQuantity<LEN, MASS, ANG, TIME, CURR, MOLE, LUMINOUS, HEAT>> for f64
{
    type Output = RQuantity<LEN, MASS, ANG, TIME, CURR, MOLE, LUMINOUS, HEAT>;
    fn mul(self, rhs: RQuantity<LEN, MASS, ANG, TIME, CURR, MOLE, LUMINOUS, HEAT>) -> Self::Output {
        RQuantity {
            value: self * rhs.value,
        }
    }
}

impl<
        const LEN: i64,
        const MASS: i64,
        const ANG: i64,
        const TIME: i64,
        const CURR: i64,
        const MOLE: i64,
        const LUMINOUS: i64,
        const HEAT: i64,
    > Mul<f64> for RQuantity<LEN, MASS, ANG, TIME, CURR, MOLE, LUMINOUS, HEAT>
{
    type Output = RQuantity<LEN, MASS, ANG, TIME, CURR, MOLE, LUMINOUS, HEAT>;
    fn mul(self, rhs: f64) -> Self::Output {
        RQuantity {
            value: self.value * rhs,
        }
    }
}

impl<
        const LEN: i64,
        const MASS: i64,
        const ANG: i64,
        const TIME: i64,
        const CURR: i64,
        const MOLE: i64,
        const LUMINOUS: i64,
        const HEAT: i64,
    > Div<RQuantity<LEN, MASS, ANG, TIME, CURR, MOLE, LUMINOUS, HEAT>> for f64
where
    RQuantity<
        { -LEN },
        { -MASS },
        { -ANG },
        { -TIME },
        { -CURR },
        { -MOLE },
        { -LUMINOUS },
        { -HEAT },
    >: Sized,
{
    type Output = RQuantity<
        { -LEN },
        { -MASS },
        { -ANG },
        { -TIME },
        { -CURR },
        { -MOLE },
        { -LUMINOUS },
        { -HEAT },
    >;
    fn div(self, rhs: RQuantity<LEN, MASS, ANG, TIME, CURR, MOLE, LUMINOUS, HEAT>) -> Self::Output {
        Self::Output {
            value: self / rhs.value,
        }
    }
}

impl<
        const LEN: i64,
        const MASS: i64,
        const ANG: i64,
        const TIME: i64,
        const CURR: i64,
        const MOLE: i64,
        const LUMINOUS: i64,
        const HEAT: i64,
    > Div<f64> for RQuantity<LEN, MASS, ANG, TIME, CURR, MOLE, LUMINOUS, HEAT>
{
    type Output = RQuantity<LEN, MASS, ANG, TIME, CURR, MOLE, LUMINOUS, HEAT>;
    fn div(self, rhs: f64) -> Self::Output {
        RQuantity {
            value: self.value / rhs,
        }
    }
}

impl<
        const LEN: i64,
        const MASS: i64,
        const ANG: i64,
        const TIME: i64,
        const CURR: i64,
        const MOLE: i64,
        const LUMINOUS: i64,
        const HEAT: i64,
    > RQuantity<LEN, MASS, ANG, TIME, CURR, MOLE, LUMINOUS, HEAT>
{
    pub fn convert(self, rhs: Self) -> f64 {
        self.value / rhs.value
    }
}

impl<
        const LEN: i64,
        const MASS: i64,
        const ANG: i64,
        const TIME: i64,
        const CURR: i64,
        const MOLE: i64,
        const LUMINOUS: i64,
        const HEAT: i64,
    > core::ops::Neg for RQuantity<LEN, MASS, ANG, TIME, CURR, MOLE, LUMINOUS, HEAT>
{
    type Output = Self;

    fn neg(self) -> Self::Output {
        Self { value: -self.value }
    }
}
