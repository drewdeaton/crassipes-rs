/*
pub struct RQuantity<
    const Length   _Dim: i64,
    const Mass     _Dim: i64,
    const Angle    _Dim: i64,
    const Time     _Dim: i64,
    const Current   _Dim: i64,
    const Chem       _Amt  _Dim: i64,
    const Luminous  _Intensity_Dim: i64,
    const Heat      _Dim: i64
> {
    pub value: f64,
}
*/

use super::RQuantity;

/// Alias for length type
#[allow(dead_code)]
pub type QLength = RQuantity<1, 0, 0, 0, 0, 0, 0, 0>;
/// Alias for mass type
#[allow(dead_code)]
pub type QMass = RQuantity<0, 1, 0, 0, 0, 0, 0, 0>;
/// Alias for angle type
#[allow(dead_code)]
pub type QAngle = RQuantity<0, 0, 1, 0, 0, 0, 0, 0>;
/// Alias for time type
#[allow(dead_code)]
pub type QTime = RQuantity<0, 0, 0, 1, 0, 0, 0, 0>;
/// Alias for current type
#[allow(dead_code)]
pub type QCurrent = RQuantity<0, 0, 0, 0, 1, 0, 0, 0>;
/// Alias for chemical amount type
#[allow(dead_code)]
pub type QChem = RQuantity<0, 0, 0, 0, 0, 1, 0, 0>;
/// Alias for luminous intensity type
#[allow(dead_code)]
pub type QLuminous = RQuantity<0, 0, 0, 0, 0, 0, 1, 0>;
/// Alias for heat type
#[allow(dead_code)]
pub type QHeat = RQuantity<0, 0, 0, 0, 0, 0, 0, 1>;

#[allow(dead_code)]
pub type QPower = RQuantity<2, 1, 0, -3, 0, 0, 0, 0>;

#[allow(dead_code)]
pub type QVoltage = RQuantity<2, 1, 0, -3, -1, 0, 0, 0>;

#[allow(dead_code)]
pub type QTorque = RQuantity<2, 1, -1, -2, 0, 0, 0, 0>;

#[allow(dead_code)]
pub type QEnergy = RQuantity<2, 1, 0, -2, 0, 0, 0, 0>;

//TODO: Add more type aliases

pub mod unit_constants {
    use crate::units::constants::*;
    /// Meter
    #[allow(dead_code)]
    pub const METER: QLength = QLength { value: 1. };
    /// Kilogram
    #[allow(dead_code)]
    pub const KILOGRAM: QMass = QMass { value: 1. };
    /// Radian
    #[allow(dead_code)]
    pub const RADIAN: QAngle = QAngle { value: 1. };
    /// Degree
    #[allow(dead_code)]
    pub const DEGREE: QAngle = QAngle { value: 0.01745329 };
    /// Second
    #[allow(dead_code)]
    pub const SECOND: QTime = QTime { value: 1. };
    /// Ampere
    #[allow(dead_code)]
    pub const AMPERE: QCurrent = QCurrent { value: 1. };
    /// Watt
    #[allow(dead_code)]
    pub const WATT: QPower = QPower { value: 1. };
    // Volt
    #[allow(dead_code)]
    pub const VOLT: QVoltage = QVoltage { value: 1. };
    // Kelvin
    #[allow(dead_code)]
    pub const KELVIN: QHeat = QHeat { value: 1. };

    //The rest of the SI base units are not gonna be defined here because mol
    // and cd arent super useful in the context of VEX
}
