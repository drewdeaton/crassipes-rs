use core::result;
use alloc::format;
use vex-rt::error::Error;
use core::fmt::*;
enum PortUseType {
    None,
    InUse,
}
static mut isPortInUse : [PortUseType; 21] = [PortUseType::None; 21];

#[cfg(feature = "no_port_verify")]
fn PortVerify(port : i8, deviceType : PortUseType) -> Result<i8, Error> {
    Ok(())
}

#[cfg(not(feature = "no_port_verify"))]
fn PortVerify(port : i8) -> Result<i8, Error> {
    if (isPortInUse[port - 1] != PortUseType::None) {
        return Err(Error{format!("Port {:?} is in use by a different device!", port)});
    }
}