use alloc::string::String;
use libc::FILE;
use libc::{c_char, fopen, fprintf};

pub struct File {
    f: *mut FILE,
}

impl File {
    fn write(&mut self, to_write: String) {
        unsafe {
            fprintf(self.f, to_write.as_ptr() as *const c_char);
        }
    }
}

impl Drop for File {
    fn drop(&mut self) {
        unsafe {
            libc::fclose(self.f);
        }
    }
}
